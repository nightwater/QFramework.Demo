﻿using QFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class ReskitDemo : MonoBehaviour
{
    public Image img;

    /// <summary>
    /// 每一个需要加载资源的单元（脚本、界面）申请一个 ResLoader
    /// ResLoader 本身会记录该脚本加载过的资源
    /// </summary>
    /// <returns></returns>
    ResLoader mResLoader = ResLoader.Allocate();

    void Awake()
    {
        // 全局只需初始化一次
        ResMgr.Init();
    }

    void Start()
    {
        //存储与读取数据常用的几个路径
        //Log.I(Application.dataPath);
        //Log.I(Application.streamingAssetsPath);
        //Log.I(Application.persistentDataPath);
        //Log.I(Application.temporaryCachePath);

        // 通过 LoadSync 同步加载资源
        // 只需要传入资源名即可，不需要传入 AssetBundle 名。
        //mResLoader.LoadSync<GameObject>("Player").Instantiate();

        //与 LoadSync 不同的是，LoadAsync异步加载是分两步的，第一步是添加到加载队列，第二步是执行异步加载。
        //这样做是为了支持同时异步加载多个资源的。
        //添加到加载队列
        //mResLoader.Add2Load("Player", (succeed, res) =>
        //{
        //    if (succeed)
        //    {
        //        res.Asset.As<GameObject>().Instantiate();
        //    }
        //});
        //// 执行异步加载
        //mResLoader.LoadAsync();

        //模拟环境下 加载图片的API  如果是正式环境，需要将相关图片打成图集 给相关图片添加packing tag
        //var s = mResLoader.LoadSprite("pic1");
        //img.sprite = s;

        //正式环境 加载图集里面的图片
        //var sa = mResLoader.LoadSync<SpriteAtlas>("test").GetSprite("pic1");
        //img.sprite = sa;

        //加载Resource文件夹下面的文件   (仅 模拟环境下 有效)
        //var sprite = mResLoader.LoadSprite("resources://pic");
        //img.sprite = sprite;

        //从网络获取图片
        var imageUrl = "http://file.liangxiegame.com/296b0166-bdea-47d5-ac87-4b55c91df16f.png";

        mResLoader.Add2Load("netimage:" + imageUrl, (succeed, res) => {
            if (succeed)
            {
                var texture2D = res.Asset as Texture2D;
                var sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.one * 0.5f);
                img.sprite = sprite;
            }
        });

        mResLoader.LoadAsync();

    }

    void OnDestroy()
    {
        // 释放所有本脚本加载过的资源
        // 释放只是释放资源的引用
        // 当资源的引用数量为 0 时，会进行真正的资源卸载操作
        mResLoader.Recycle2Cache();
        mResLoader = null;
    }
}
